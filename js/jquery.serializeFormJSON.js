/**
 * Created with JetBrains PhpStorm.
 * User: cd
 * Date: 11/15/12
 * Time: 5:31 PM
 * To change this template use File | Settings | File Templates.
 */
$.fn.serializeFormJSON = function() {

    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
