/**
 * Created with JetBrains PhpStorm.
 * User: cd
 * Date: 11/15/12
 * Time: 5:41 PM
 * To change this template use File | Settings | File Templates.
 */

RAjaxModal = function(css_selector, url, indicatorUrl,title) {

    this.url = url;
    this.selector = css_selector;
    this.indicatorUrl = indicatorUrl;
    this.params = '';
    this.modal = '';
    this.iconEditStr = '';
    this.title = title || 'Popup has no title';

    console.log(this.url);

    this.init = function() {
        var tthis = this;
        $(this.selector).click(function(e) {
            tthis.params = $(this).attr('params');
            e.preventDefault();
            tthis.iconEditStr = $('<p></p>');
            tthis.iconEditStr.append('<h3>Loading...</h3>');
            tthis.iconEditStr.append('<img src="'+tthis.indicatorUrl+'" />');
            tthis.modal = bootbox.modal(tthis.iconEditStr, tthis.title );
            tthis.iconEditStr.load(tthis.url, {params : tthis.params});
        });

    };

    this.close = function()
    {
        this.modal.modal('hide');
    };

    this.processSpecialResponse = function(data)
    {
        if ( data['action'] === 'close' )
        {
            this.close();
            return;
        }

        if ( data['action'] === 'reload' )
        {
            window.location.reload();
            return;
        }
    };

    this.submit = function(form_selector)
    {
        data = $(form_selector).serializeFormJSON();
        data['params'] = this.params;
        console.debug('Serialized data',data);
        this.iconEditStr.html('');
        var tthis = this;
        this.modal.css('max-height','');
        this.iconEditStr.load(this.url,data, function(response, status, xhr)
        {
            if ( xhr.status == 204 )
            {
                eval('x =' + xhr.statusText);
                tthis.processSpecialResponse(x);
            }
            return false;
        });
        return false;
    };

    return this;
};
