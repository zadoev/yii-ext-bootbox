<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zadoev@gmail.com
 * Date: 11/14/12
 * Time: 4:42 PM
 *
 */

class AjaxModalWidget extends CWidget
{

    public $triggerSelector = '';
    public $apiUrl = '';
    public $ajaxLoaderInicatorUrl = '';
    public $specialResponseProcessor = '';

    public $title = '';

    public static $widgetCounter=1;



    public $widgetVariableName = 'ajaxModalObject';

    public function init()
    {
        parent::init();

        $indicatorImageFile = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'/ajax-loader.gif';
        $path = Yii::app()->getAssetManager()->publish($indicatorImageFile);

        $this->ajaxLoaderInicatorUrl = $path;
    }

    /**
     * @todo: minimize it, in dev mode include full js, in prod include minimized and mapped scripts
     */
    public function run()
    {
        $overrideProcessor = '';
        if ( $this->specialResponseProcessor )
            $overrideProcessor = $this->widgetVariableName.'.processSpecialResponse = ' . $this->specialResponseProcessor.';';

        $bootBoxJsFile = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'/bootbox.min.js';
        $path = Yii::app()->getAssetManager()->publish($bootBoxJsFile);
        Yii::app()->clientScript->registerScriptFile($path, CClientScript::POS_END);

        $serializeFormJSONJsFile = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'/jquery.serializeFormJSON.js';
        $path = Yii::app()->getAssetManager()->publish($serializeFormJSONJsFile);
        Yii::app()->clientScript->registerScriptFile($path, CClientScript::POS_END);


        $rAajxModalJsFilePath = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'/RAjaxModal.js';
        $path = Yii::app()->getAssetManager()->publish($rAajxModalJsFilePath);
        Yii::app()->clientScript->registerScriptFile($path, CClientScript::POS_END);


        Yii::app()->clientScript->registerScript(self::$widgetCounter++.'modalWidget', <<<ADDONS

            var {$this->widgetVariableName} = new RAjaxModal("{$this->triggerSelector}",'{$this->apiUrl}', '{$this->ajaxLoaderInicatorUrl}', '{$this->title}');

            {$this->widgetVariableName}.init();

            {$overrideProcessor};

ADDONS
            , CClientScript::POS_END);


    }
}
